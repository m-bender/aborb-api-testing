$(document).ready(function(){
  $('#add-user').click(function(e){
    e.preventDefault();
    $.ajax({
      type: "POST",
      url: "/api.php",
      data: {
        action:'add_user',
        fname: $('#fname1').val(),
        lname: $('#lname1').val(),
        email: $('#email1').val(),
        course_id: $('#courseid1').val(),
      },
    })
    .done(function(r){
      if(r.message) alert(r.message);
    });
  });
  $('#get-grade').click(function(e){
    e.preventDefault();
    $.ajax({
      type: "POST",
      url: "/api.php",
      data: {
        action:'get_grade',
        email: $('#email2').val(),
        course_id: $('#courseid2').val(),
      },
    })
    .done(function(r){
      if(r.message) alert(r.message);
    });
  });
  $('#sign-in').click(function(e){
    e.preventDefault();
    $.ajax({
      type: "POST",
      url: "/api.php",
      data: {
        action:'sign_in',
        email: $('#email3').val(),
      },
    })
    .done(function(r){
      if(r.message == 'saml') {
        var xml = r.data;
        var $form = (
          $("<form/>")
            .attr("action", API_ENDPOINT+"/account/saml")
            .attr("method", "POST")
            .append(
              $("<input/>")
                .attr("type","hidden")
                .attr("name","samlresponse")
                .val(xml)
            )
        );
        if($("#courseid3").val() != "") {
          $form.append(
            $("<input/>")
              .attr("type","hidden")
              .attr("name","relaystate")
              .val(API_ENDPOINT+"/#/online-courses/"+COURSE_ID)
          );
        }
        $("html").append($form);
        $form.submit();
      }
      else if(r.message) alert(r.message);
    });
  });
});
