<?php
require("./vendor/autoload.php");
// Loads enviroment variables from .env
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>ePath Migration Testing</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="style.css">
  <script type="text/javascript">
    const API_ENDPOINT = '<?php echo $_ENV['BASE_URL']; ?>';
    const COURSE_ID = '<?php echo $_ENV['COURSE_ID'];?>'
  </script>
</head>
<body>
<div class="jumbotron">
  <h1 class="display-4">Absorb API Testing</h1>
  <p class="lead">Simple tool to test the API integration for migration from ePath</p>

  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#add" role="tab" aria-controls="add" aria-selected="true">Add & Enroll User</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#grade" role="tab" aria-controls="grade" aria-selected="false">Get Grade</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#signon" role="tab" aria-controls="signon" aria-selected="false">Single Sign In</a>
    </li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane fade show active" id="add" role="tabpanel" aria-labelledby="add-user-tab">
      <form>
        <div class="form-group">
          <label for="fname1">First Name</label>
          <input type="text" class="form-control" id="fname1" name="fname">
        </div>
        <div class="form-group">
          <label for="lname1">Last Name</label>
          <input type="text" class="form-control" id="lname1" name="lname">
        </div>
        <div class="form-group">
          <label for="email1">Email Address</label>
          <input type="email" class="form-control" id="email1" name="email">
        </div>
        <div class="form-group">
          <label for="courseid">Course ID</label>
          <input type="text" class="form-control" id="courseid1" name="course_id" value="<?php echo $_ENV['COURSE_ID'];?>">
        </div>
        <div class="form-group">
          <label for="department">Department</label>
          <input type="text" class="form-control" id="department" value="<?php echo $_ENV['ABSORB_DEPT_ID'];?>">
        </div>
        <a id="add-user" class="btn btn-primary btn-lg" href="#" role="button">Add & Enroll User</a>
      </form>
    </div>
    <div class="tab-pane fade" id="grade" role="tabpanel" aria-labelledby="grade-user-tab">
      <form>
        <div class="form-group">
          <label for="email2">Email Address</label>
          <input type="email" class="form-control" id="email2" name="email">
        </div>
        <div class="form-group">
          <label for="courseid2">Course ID</label>
          <input type="text" class="form-control" id="courseid2" name="course_id" value="<?php echo $_ENV['COURSE_ID'];?>">
        </div>
        <a id="get-grade" class="btn btn-primary btn-lg" href="#" role="button">Get Grade</a>
      </form>
    </div>
    <div class="tab-pane fade" id="signon" role="tabpanel" aria-labelledby="generate-single-signon-user-tab">
      <form>
        <div class="form-group">
          <label for="email3">Email Address</label>
          <input type="email" class="form-control" id="email3" name="email">
        </div>
        <div class="form-group">
          <label for="courseid3">Course ID (optional)</label>
          <input type="text" class="form-control" id="courseid3" name="course_id" value="<?php echo $_ENV['COURSE_ID'];?>">
        </div>
        <a id="sign-in" class="btn btn-primary btn-lg" href="#" role="button">Sign In</a>
      </form>
    </div>
  </div>


</div>

  <script
    src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="app.js?v=1.3"></script>
</body>
</html>

