<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require("./vendor/autoload.php");
// Loads enviroment variables from .env
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

function processRequest() {
  if(!isset($_REQUEST['action'])) {
    http_response_code(400);
    return ['message'=>'Action required'];
  }
  require("./AbsorbAPIClient.php");

  $api = new AbsorbAPIClient([
    'absorb_private_key'    => $_ENV['ABSORB_PRIVATE_KEY'],
    'absorb_admin_username' => $_ENV['ABSORB_ADMIN_USERNAME'],
    'absorb_admin_password' => $_ENV['ABSORB_ADMIN_PASSWORD'],
    'absorb_dept_id'        => $_ENV['ABSORB_DEPT_ID'],
    'base_url'              => $_ENV['BASE_URL'],
    'course_id'             => $_ENV['COURSE_ID'],
    'single_sign_on' => [
      'issuer'      => $_ENV['SINGLE_SIGN_ON_ISSUER'],
      'certificate' => __DIR__.'/keys/'.$_ENV['SINGLE_SIGN_ON_CERTIFICATE'],
      'private_key' => __DIR__.'/keys/'.$_ENV['SINGLE_SIGN_ON_PRIVATE_KEY'],
    ]
  ]);
  $api->register();

  switch($_REQUEST['action']) {
    case "add_user":
      $absorbUser = $api->user_exists(['email'=>$_REQUEST['email']]);
      if($absorbUser != false) return ['message' => 'User already exists'];
      $absorbUser = $api->create_user([
        "FirstName"		=> $_REQUEST['fname'],
        "LastName"		=> $_REQUEST['lname'],
        "Username"		=> $_REQUEST['email'],
        "Password"		=> md5(rand()),
        "EmailAddress"	=> $_REQUEST['email'],
        "DepartmentId" => $_ENV['ABSORB_DEPT_ID']
      ]);
      if($absorbUser === false) return ['message' => 'Error creating user'];
      $api->enroll([
        'user_id' =>  $absorbUser->Id,
        'course_id' => $_REQUEST['course_id']
      ]);
      return ['message' => 'User created and enrolled'];
      break;
    case "get_grade":
      $absorbUser = $api->user_exists(['email'=>$_REQUEST['email']]);
      if($absorbUser === false) return ['message' => 'user does not exist'];
      $grade = $api->get_grade($absorbUser->Id, $_REQUEST['course_id']);
      if(!is_numeric($grade)) 
        return ['message' => 'User does not have grade for course '.$_REQUEST['course_id']];
      else
        return ['message' => 'User\'s grade is '.$grade];
      break;
    case "sign_in":
      $absorbUser = $api->user_exists(['email'=>$_REQUEST['email']]);
      if($absorbUser === false) return ['message' => 'user does not exist'];

      $saml_xml = $api->CreateSamlResponse(array('userName' => $absorbUser->Username));
      return ['message' => 'saml', 'data'=>base64_encode($saml_xml)];
      break;
  }
}

header('Content-Type: application/json');
try {
  echo json_encode(processRequest());
}
catch(\Exception $e) {
  echo json_encode(['message' => $e->getMessage()]);
}
