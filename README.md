# Absorb API Integration

## Introduction
This is a simple web app that demonstrates a basic integration with Aborb's API and SAML Single Sign on.

> :warning: **DONT HOST THIS ON A WEB SEVER**: This tool is for testing locally. If you run this on a public facing web server, you would be giving any miscreant the ability to enroll themselves into courses and impersonate users.

## Requirements
PHP: php.net

Composer: https://getcomposer.org/

## Installation

- Clone this repository
- Run `composer install` to install the require PHP libraries
- Generate a key pair for Single Sign On
- Configure Absorb LMS for SSO
- Create the Environment file.

## Key Pair Generation

The easiest way to create a key pair is using openssl:

`$ openssl req -new -x509 -days 365 -nodes -sha256 -out saml.crt -keyout saml.pem`

Once generated, the keys should be placed in the keys directory.

For more information, see here: https://www.lightsaml.com/LightSAML-Core/Cookbook/How-to-generate-key-pair/

## Configuring Absorb for SSO

Using and administrative account, login to Absorb. pca.myabsorb.com/admin

Once logged in, click on your profile icon in the top right hand corner, and then navigate to Portal Settings > Manage Single Sign-On Settings, and the click Add.

**Name** : Enter the name of your company

**Method**: Select SAML

**Key**: Enter your certificate as one line without the -----BEGIN CERTIFICATE-----  or -----END CERTIFICATE-----.

**Mode**: Service Provider Initiated

**ID Property**: Username

**Signature Typ**e: Sha256

**Login UR**L: The url for your LMS

**Logout UR**L: Left Blank

**Assigned Route**: The route provided to you by PCA.


Click Save.

I used the following article as a reference when configuring Absorb: https://support.absorblms.com/hc/en-us/articles/115005251488-Incoming-SAML-2-0-Single-Sign-On-with-G-Suite

## Create an environment file

Copy the sample.env to .env

COURSE_ID: This is the default course for enrollment and redirection on login.

ABSORB_PRIVATE_KEY: The API key provided to you by PCA

ABSORB_ADMIN_USERNAME: Your Absorb administrative username

ABSORB_ADMIN_PASSWORD: Your Absorb administrative password

ABSORB_DEPT_ID: Default department  used when adding users.

BASE_URL:  The route provided to you by PCA (e.g. https://myroute.myabsorb.com).

SINGLE_SIGN_ON_ISSUER: Issuer used when generating the key pair.

SINGLE_SIGN_ON_CERTIFICATE: saml.crt

SINGLE_SIGN_ON_PRIVATE_KEY: saml.pem

## Running the Application

Current versions of PHP have a built in web server. From a command line, run:

```bash
php -S localhost:8080 .
```

## Credit

This app is based off of Grefory Pymm's Wordpress plugin which can be found here: https://github.com/gregpymm/absorb_lms_api_client
